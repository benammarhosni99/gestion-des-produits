from django.db import models

# Create your models here.
class Product:
    bar_code = models.CharField(primary_key=True)
    product_name = models.CharField(max_length=200)
    qte = models.IntegerField()